VAR_PROJECT = des
VAR_COMPOSE_FILES = -f laradock/docker-compose.yml -f docker-compose.yml
VAR_DEV_SERVICES = nginx mysql redis node php-worker phpmyadmin

rundev:
	docker-compose -p $(VAR_PROJECT) $(VAR_COMPOSE_FILES) up $(VAR_DEV_SERVICES)

rundev_d:
	docker-compose -p $(VAR_PROJECT) $(VAR_COMPOSE_FILES) up -d $(VAR_DEV_SERVICES)

build_and_rundev:
	docker-compose -p $(VAR_PROJECT) $(VAR_COMPOSE_FILES) up --build $(VAR_DEV_SERVICES)

build_and_rundev_d:
	docker-compose -p $(VAR_PROJECT) $(VAR_COMPOSE_FILES) up -d --build $(VAR_DEV_SERVICES)

stopdev:
	docker-compose -p $(VAR_PROJECT) $(VAR_COMPOSE_FILES) stop

rm:
	docker-compose -p $(VAR_PROJECT) $(VAR_COMPOSE_FILES) rm 

workspace:
	docker exec -it --user=laradock des_workspace bash

restart_workers:
	docker restart $$(docker ps -q -f "label=queue-worker")

scale_workers:
	docker-compose -p $(VAR_PROJECT) $(VAR_COMPOSE_FILES) scale php-worker=${n}
