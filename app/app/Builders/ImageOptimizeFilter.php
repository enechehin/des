<?php

namespace App\Builders;

use Intervention\Image\Filters\FilterInterface;
use Approached\LaravelImageOptimizer\ImageOptimizer;

class ImageOptimizeFilter implements FilterInterface
{
    

    /**
     * Applies filter effects to given image
     *
     * @param  \Intervention\Image\Image $image
     * @return \Intervention\Image\Image
     */
    public function applyFilter(\Intervention\Image\Image $image)
    {

    	$imageOptimizer = new ImageOptimizer();
        $imageOptimizer->optimizeImage($image->basePath());

        return $image;
    }

}