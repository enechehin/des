<?php

namespace App\Builders;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use App\Models\Asset;
use App\Models\Assets\Image as AssetImage;
use Intervention\Image\Facades\Image;

class ImageBuilder {


    private $asset;


    function __construct(Asset $asset) {
        $this->asset = $asset;
    }


    /**
     * Create Image model from file
     * 
     * @param $filePath string
     * @return \App\Models\Image
     */
    public function createFromFile(string $filePath) 
    {
        $assetImage = AssetImage::create([
            'project_id' => $this->asset->project_id,
            'asset_id' => $this->asset->id
        ]);
        
        $pathname = Storage::putFile($assetImage->path(), new File($filePath));
        
        $assetImage->name = pathinfo($pathname, PATHINFO_BASENAME);
        
        return $this->postProcess($assetImage);
        


        
//        $assetImage->save();
        

        // $imageSize = getimagesize($filePath);
        // $width = $imageSize[0];
        // $height = $imageSize[1];
        // $fileSize = filesize($filePath);

        // $imagePath = $this->moveToStorage($filePath);

        // $assetImage = AssetImage::create([
        //     'project_id' => $this->asset->project_id,
        //     'asset_id' => $this->asset->id,
        //     'width' => $width,
        //     'height' => $height,
        //     'ratio' => $this->calculateRatio($width, $height),
        //     'color' => 'ffffff',
        //     // 'size' => $fileSize
        // ]);

//        return $assetImage;
    }
    
    
    private function postProcess(AssetImage $assetImage) {
        
        $image = Image::make($assetImage->fullyQualifiedPathname());
        
//        Log::debug('identifyFormat', [
//            'opaque' => $image->getCore()->identifyFormat("%[opaque]"),
//            'mime' => $image->mime()
//        ]);
//        
//        if ($image->mime() === 'image/png' && $image->getCore()->identifyFormat("%[opaque]") === 'true') {
//            $assetImage->name = basename($assetImage->name, '.png') . '.jpg';
//            if ($image->save($assetImage->fullyQualifiedPathname())) {
//                $assetImage->save();
//            }
//        }
        
        // optimize image
        $image->filter(new ImageOptimizeFilter());
        
        
        // fill attributes
        $assetImage->width = $image->width();
        $assetImage->height = $image->height();
        $assetImage->ratio = $this->calculateRatio($assetImage->width, $assetImage->height);
        $assetImage->size = $image->filesize();
        
        $assetImage->save();
        
        // clear
        $image->destroy();
        
        return $assetImage;
    }



    private function calculateRatio($width, $height) {
        return round($height/($width/100), 2);
    }


}