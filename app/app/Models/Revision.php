<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    

    const STATUS_OK = 1;

    const STATUS_UPLOADING = 2;
    const STATUS_UPLOADED = 3;

    const STATUS_EXTRACTING = 10;
    const STATUS_DELETING = 20;

    const STATUS_ERROR_UPLOAD = 101;    
    const STATUS_ERROR_VALIDATE = 102;


    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'pageview_id', 'version', 'status', 'sources', 'asset_id', 'created_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['project_id', 'pageview_id', 'user_id', 'version', 'status'];


     /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['sources'];


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => \App\Events\Revision\Created::class,
        'saved' => \App\Events\Revision\Updated::class,
        'deleting' => \App\Events\Revision\Deleting::class,
    ];


	/**
     * Get revision view
     */
    public function view()
    {
        return $this->belongsTo('App\Models\PageView', 'pageview_id');
    }


    /**
     * Get revision project
     */
    public function project()
    { 
        return $this->belongsTo('App\Models\Project');
    }


    /**
     * Get available for revision sources
     */
    public function sources() 
    {
        return $this->hasMany('App\Models\Source');
    }


    /**
     * Get revision asset
     * 
     * @return \Illuminate\Database\Relation\BelongsTo
     */
    public function asset() {
        return $this->belongsTo('App\Models\Asset');
        // return $this->hasOne('\App\Models\Image');
    }

}
