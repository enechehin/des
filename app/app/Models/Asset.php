<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['project_id'];
    
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'images'];
    
    
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['images'];
    
    
    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => \App\Events\Asset\Created::class,
        'saved' => \App\Events\Asset\Updated::class,
        'deleting' => \App\Events\Asset\Deleting::class,
    ];


    /**
     * Assets folder path
     * 
     * @return string
     */
    public function path() {
        return 'assets/' . dechex($this->project_id) . '/' . dechex($this->id);
    }
    
    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function revision() {
        return $this->hasOne('App\Models\Revision');
    }

    
    /**
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images() {
        return $this->hasMany('App\Models\Assets\Image');
    }


    public function colors() {

    }


    public function fonts() {

    }

}
