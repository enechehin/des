<?php

namespace App\Models\Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 
    ];


    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name'];


    public function roles() {
        return $this->belongsToMany('App\Models\Auth\Role')->withTimestamps();
    }

    
    public function hasRole($name) {
        
        foreach ($this->roles as $role) {
            if ($role->name === $name) {
                return true;
            }
        }

        return false;
    }

    
    public function assignRole($role) {
        return $this->roles()->attach($role);
    }

    
    public function removeRole($role) {
        return $this->roles()->detach($role);
    }
    
    
    public function social() {
        return $this->hasMany('App\Models\Auth\Social');
    }
    
    
    public function projects() {
        return $this->belongsToMany('App\Models\Project');
    }

}
