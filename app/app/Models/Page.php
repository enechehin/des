<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {


    const STATUS_OK = 1;
    const STATUS_DELETING = 2;
    const STATUS_ERROR_DELETING = 101;


    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'title', 'status', 'views'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'status', 'user_id'];


    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['views'];


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => \App\Events\Page\Created::class,
        'saved' => \App\Events\Page\Updated::class,
        'deleting' => \App\Events\Page\Deleting::class,
    ];


    /**
     * Get creator user
     */
    public function user() {
    	return $this->belongsTo('App\Models\Auth\User');
    }


    /**
     * Get page project
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }


    /**
     * Get page views
     */
    public function views()
    {
        return $this->hasMany('App\Models\PageView');
    }

}