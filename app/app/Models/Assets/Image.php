<?php

namespace App\Models\Assets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'project_id', 'asset_id', 'width', 'height', 'ratio', 'size', 'color'];

    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'height', 'width', 'ratio', 'url'];

    
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url'];
    
    
    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => \App\Events\AssetImage\Created::class,
        'saved' => \App\Events\AssetImage\Updated::class,
        'deleting' => \App\Events\AssetImage\Deleting::class,
    ];


    /**
     * Get image folder path
     * 
     * @return string
     */
    public function path() {
        return 'assets/' . dechex($this->project_id) . '/' . dechex($this->asset_id);
    }


    /**
     * Get image pathname
     * 
     * @return string
     */
    public function pathname() {
        return $this->path() . '/' . $this->name;
    }
    
    
    public function fullyQualifiedPathname() {
        return storage_path('app') . '/' . $this->pathname();
    }


    public function getUrlAttribute()
    {
        return '/' . $this->pathname();
//    	return Storage::url($this->pathname());
    }

}
