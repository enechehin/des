<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageView extends Model
{

    const STATUS_OK = 1;
    const STATUS_OK_UPDATING = 2;
    const STATUS_DELETING = 3;
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'page_id', 'title', 'status', 'count_revisions', 'latest_revision'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['project_id', 'page_id', 'title', 'user_id', 'status'];


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => \App\Events\PageView\Created::class,
        'saved' => \App\Events\PageView\Updated::class,
        'deleting' => \App\Events\PageView\Deleting::class,
    ];


    /**
     * Get view page
     */
    public function page()
    {
        return $this->belongsTo('App\Models\Page');
    }


    /**
     * Get page project
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }


    /**
     * Get creator user
     */
    public function user() {
    	return $this->belongsTo('App\Models\Auth\User');
    }


    /**
     * Get pageview revisions
     */
    public function revisions()
    {
        return $this->hasMany('App\Models\Revision', 'pageview_id');
    }
    

}
