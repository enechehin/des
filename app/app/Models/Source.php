<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    
    const DISK_LOCAL = 1;
    

    protected $fillable = [
        'project_id', 
        'revision_id', 
        'disk', 
        'title', 
        'mime_type',
        'size',
        'hash'
    ];


    protected $visible = ['id', /*'project_id', */'revision_id'];


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => \App\Events\Source\Created::class,
        'saved' => \App\Events\Source\Updated::class,
        'deleting' => \App\Events\Source\Deleting::class,
    ];
    
    
    /**
     * Get source folder path
     * 
     * @return string
     */
    public function path() {
        return 'sources/' . dechex($this->project_id);
    }


    /**
     * Get source pathname
     * 
     * @return string
     */
    public function pathname() {
        return $this->path() . '/' . $this->name;
    }
    
    
    public function fullyQualifiedPathname() {
        return storage_path('app') . '/' . $this->pathname();
    }


    /**
     *  Get source revision model
     */
    public function revision() {
        return $this->belongsTo('App\Models\Revision');
    }
    
}
