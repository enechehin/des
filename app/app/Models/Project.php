<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {


    const STATUS_OK = 1;
    

	/**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'title', 'status'];


    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => \App\Events\Project\Created::class,
        'saved' => \App\Events\Project\Updated::class,
        'deleting' => \App\Events\Project\Deleted::class,
    ];


    public function users() {
        return $this->belongsToMany('App\Models\Auth\User');
    }


    public function pages() {
        return $this->hasMany('App\Models\Page');
    }
    
    
    // public function getLink() {
    //     return route('app', [ 
    //         'projectId' => $this->id 
    //     ]);
    // }

}