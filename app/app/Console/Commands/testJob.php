<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Storage;
use App\Jobs\ExtractAssets;
use App\Models\Revision;
use Illuminate\Http\File;


class testJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test ProcessUploadedRevision job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // var_dump(Storage::get('uploads/PcdnagU0V35JpaRThl1bal93jB2HkyYMINGGruSl.psd'));exit;
        // var_dump(Storage::disk('temp')->url('uploads/GfmxhYHy2DKwLExI5lGWZ4GQg4NwHudhczhrJ3Ki.jpeg'));exit;
        $rev = Revision::find(3);
        dispatch(new ExtractAssets($rev));
    }
}
