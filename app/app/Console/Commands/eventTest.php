<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class eventTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test pushstream event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // event(new \App\Events\Project\Updated(\App\Models\Project::find(5)));
        //event(new \App\Events\Project\Created(\App\Models\Project::find(1)));
        event(new \App\Events\Page\Created(\App\Models\Page::find(1)));
    }
}
