<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreRevisionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        $pageView = $this->route('pageView');
 
        if ($pageView) {
            return $pageView->project->users()->exists( Auth::user() );
        }

        $revision = $this->route('revision');
 
        if ($revision) {
            return $revision->project->users()->exists( Auth::user() );
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pageview_id' => 'required|integer',
            'status' => 'required|integer',
        ];
    }
}
