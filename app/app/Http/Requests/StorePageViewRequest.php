<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StorePageViewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $page = $this->route('page');
        
        if ($page) {
            return $page->project->users()->exists( Auth::user() );
        }

        $pageView = $this->route('pageView');
 
        if ($pageView) {
            return $pageView->project->users()->exists( Auth::user() );
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_id' => 'required|integer',
            'title' => 'required|max:255',
        ];
    }
}
