<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use JildertMiedema\LaravelPlupload\Facades\Plupload;

use App\Models\Revision;
use App\Models\Source;
use App\Jobs\ExtractAssets;
// use App\Events\Revision\Uploaded;

class SourceController extends Controller
{
    

    /**
     * Hanlde new revision upload
     *
     * @param $request \Illuminate\Http\Request
     * @param $revisionId int
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request, $revisionId)
    {
        return Plupload::receive('file', function (UploadedFile $file) use ($revisionId) {

            Log::info('Source.upload: user file uploaded', [
                'client_revision_id' => $revisionId,
                'user_id' => Auth::user()->id
            ]);

            $revision = Revision::find($revisionId);

            if (!$revision) {
                Log::error('Source.upload: revision not found, deleting uploaded file', [
                    'user_id' => Auth::user()->id,
                    'client_revision_id' => $revisionId,
                ]);
                Storage::delete($file);
                return 'error';
            }

            if (!$revision->project->users->contains(Auth::user())) {
                Log::error('Source.upload: user not permited uplaod for revision, deleting uploaded file', [
                    'user_id' => Auth::user()->id,
                    'revision_id' => $revision->id,
                ]);
                Storage::delete($file);
                return 'error';
            }

            if ($revision->status != Revision::STATUS_UPLOADING) {
                Log::error('Source.upload: revision status not is uploading, deleting uploaded file', [
                    'user_id' => Auth::user()->id,
                    'revision_id' => $revision->id,
                    'status' => $revision->status
                ]);
                Storage::delete($file);
                return 'error';
            }
                
            $source = $revision->sources()->create([
                'project_id' => $revision->project_id,
                'disk' => Source::DISK_LOCAL,
                'title' => $file->getClientOriginalName(),
                'mime_type' => $file->getMimeType(),
                'size' => $file->getSize(),
                'hash' => md5_file($file->path())
            ]);
            
            $newPathName = $file->store($source->path());
            
            $source->name = pathinfo($newPathName, PATHINFO_BASENAME);
            
            $source->save();

            Log::info('Source uploaded', [
                'id' => $source->id,
                'revision_id' => $revision->id,
                'user_id' => Auth::user()->id
            ]);

            if ($revision->status === Revision::STATUS_UPLOADING) {

                $revision->status = Revision::STATUS_UPLOADED;
                $revision->save();

                $extractAssetsJob = new ExtractAssets($revision);
                $extractAssetsJob->onConnection('redis');
                
                dispatch($extractAssetsJob);
            }

            return 'ready';
        });
    }


    /**
     * Download revision source
     */
    public function download(Revision $revision) {

        if (!$revision->project->users->contains(Auth::user())) {
            Log::error('User not permited for download revision source', [
                'revision_id' => $revisionId,
                'user_id' => Auth::user()->id
            ]);
            abort(403);
        }

        $source = $revision->sources()->orderBy('disk')->first();

        if (empty($source)) {
            abort(404);
        }

        switch ($source->disk) {
            case Source::DISK_LOCAL:
                return $this->downloadFromLocalDisk($source);
                break;
            
            default:
                return 'ddd';
        }
        // if () {

        // }
        // var_dump($source);

    }


    private function downloadFromLocalDisk(Source $source) {
        return response('')
            ->header('X-Accel-Redirect', '/internal-download/' . $source->pathname())
            ->header('Content-Type', $source->mime_type)
            ->header('Content-Disposition', 'attachment; filename="' . $source->title . '"');
    }

}
