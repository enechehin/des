<?php

namespace App\Http\Controllers;

// use App\Models\Page;
use App\Models\PageView;
use App\Models\Revision;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\StoreRevisionRequest;
use App\Jobs\DeleteRevision;

class ApiRevisionController extends Controller 
{


    /**
     * Get page views
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function index(PageView $pageView) {
            return $pageView->revisions()->with('sources')->get();
    }


	/**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRevisionReques t  $request
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function create(StoreRevisionRequest $request, PageView $pageView)
    {

        $version = 1;

        if ($pageView->revisions->count()) {
            $version = $pageView->revisions->last()->version + 1;
        }

        $revision = $pageView->revisions()->create([
            'user_id' => Auth::user()->id,
            'project_id' => $pageView->project_id,
            'version' => $version,
            'status' => $request->input('status'),
        ]);
        
        Log::info('User ' . Auth::user()->id . ' create revision ' . $revision->id . ' of pageview ' . $pageView->id);

        return $revision;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreRevisionRequest  $request
     * @param  \App\Models\PageView $pageView
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRevisionRequest $request, Revision $revision)
    {
        $revision->status = $request->input('status');
        $revision->save();
        
        Log::info('User ' . Auth::user()->id . ' update revision ' . $revision->id);

        return $revision;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PageView  $pageView
     * @return \Illuminate\Http\Response
     */
    public function destroy(Revision $revision)
    {
        if (!$revision->project->users->contains(Auth::user())) {
            abort(403);
        }
        
        Log::info('Delete revision request', [
            'revision_id' => $revision->id,
            'user_id' => Auth::user()->id
        ]);
        
        if ($revision->status === Revision::STATUS_DELETING) {
            
            Log::warning('Delete revision request canceled - already deleting', [
                'revision_id' => $revision->id,
                'user_id' => Auth::user()->id
            ]);
            
            return response()->json([
                'success' => false
            ]);
        }
                
        $deleteRevisionJob = new DeleteRevision($revision);
        $deleteRevisionJob->onConnection('redis');
        
        dispatch($deleteRevisionJob);
        
        $revision->status = Revision::STATUS_DELETING;
        $revision->save();

//        $revision->delete();
//        
//        event(new \App\Events\PageView\RevisionsUpdated($revision->view));
        
        return response()->json([
            'success' => true
        ]);
    }



    public function asset(Revision $revision) {
        return $revision->asset;
//        return $revision->asset->toJson();
    }

}