<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\StorePageRequest;
use App\Jobs\DeletePage;

class ApiPageController extends Controller 
{


	/**
	 * Get project pages
	 *
	 * @param  \App\Models\Project  $project
	 * @return \Illuminate\Http\Response
	 */
	public function index(Project $project) {
		return $project->pages()->get();
	}


	/**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePageRequest  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function create(StorePageRequest $request, Project $project)
    {
        $page = $project->pages()->create([
            'title' => $request->input('title'),
            'user_id' => Auth::user()->id,
            'status' => Page::STATUS_OK
        ]);

        if ($request->input('recipe')) {
            $this->precreatePageView($page, $request->input('recipe'));
        }

        Log::info('User create page ', [
            'page_id' => $page->id,
            'user_id' => Auth::user()->id
        ]);
        
        return response()->json([
            'success' => true,
            'data' => $page
        ]);
    }


    private function precreatePageView(Page $page, $recipe) {
        $views = [];

        switch ($recipe) {
            case '1':
                $views = [ 'Default' ];
                break;

             case '2':
                $views = [ 'Mobile', 'Tablet', 'Desktop' ];
                break;   
            
            default:
                # code...
                break;
        }

        if (!empty($views)) {
            foreach ($views as $view) {
                $page->views()->create([
                    'project_id' => $page->project_id,
                    'user_id' => Auth::user()->id,
                    'title' => $view
                ]);
            }
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StorePageRequest  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(StorePageRequest $request, Page $page)
    {
        $page->title = $request->input('title');
        $page->save();

        Log::info('User update page ', [
            'page_id' => $page->id,
            'user_id' => Auth::user()->id
        ]);
        
        return response()->json([
            'success' => true,
            'data' => $page
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        if (!$page->project->users->contains(Auth::user())) {
            Log::warning('User try to delete not permitted page', [
                'page_id' => $page->id,
                'user_id' => Auth::user()->id
            ]);
            abort(403);
        }

        if ($page->status === Page::STATUS_DELETING) {
            
            Log::warning('Delete page request canceled - already deleting', [
                'page_id' => $page->id,
                'user_id' => Auth::user()->id
            ]);
            
            return response()->json([
                'success' => false
            ]);
        }

        Log::info('Delete page request', [
            'page_id' => $page->id,
            'user_id' => Auth::user()->id
        ]);
        
        $deletePageJob = new DeletePage($page);
        $deletePageJob->onConnection('redis');
        
        dispatch($deletePageJob);
        
        $page->status = Page::STATUS_DELETING;
        $page->save();
        
        return response()->json([
            'success' => true
        ]);
    }

}