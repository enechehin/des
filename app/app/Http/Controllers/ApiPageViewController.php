<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\PageView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\StorePageViewRequest;
use App\Jobs\DeletePageView;

class ApiPageViewController extends Controller 
{


	/**
	 * Get page views
	 *
	 * @param  \App\Models\Project  $project
	 * @return \Illuminate\Http\Response
	 */
	public function index(Page $page) {
		return $page->views;
	}


	/**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePageViewRequest  $request
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function create(StorePageViewRequest $request, Page $page)
    {
        $pageView = $page->views()->create([
            'title' => $request->input('title'),
            'user_id' => Auth::user()->id,
            'project_id' => $page->project_id,
            'status' => PageView::STATUS_OK
        ]);

        Log::info('User create pageview', [
            'user_id' => Auth::user()->id,
            'pageview_id' => $pageView->id
        ]);
        
        return response()->json([
            'success' => true,
            'data' => $pageView
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StorePageViewRequest  $request
     * @param  \App\Models\PageView $pageView
     * @return \Illuminate\Http\Response
     */
    public function update(StorePageViewRequest $request, PageView $pageView)
    {
        $pageView->title = $request->input('title');
        $pageView->save();
        
        Log::info('User update pageview', [
            'user_id' => Auth::user()->id,
            'pageview_id' => $pageView->id
        ]);

        return response()->json([
            'success' => true,
            'data' => $pageView
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PageView  $pageView
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageView $pageView)
    {
        if (!$pageView->project->users->contains(Auth::user())) {
            abort(403);
        }
        
        Log::info('Delete pageview request', [
            'pageview_id' => $pageView->id,
            'user_id' => Auth::user()->id
        ]);
        
        if ($pageView->status === PageView::STATUS_DELETING) {
            
            Log::warning('Delete pageview request canceled - already deleting', [
                'pageview_id' => $pageView->id,
                'user_id' => Auth::user()->id
            ]);
            
            return response()->json([
                'success' => false
            ]);
        }
                
        $deletePageViewJob = new DeletePageView($pageView);
        $deletePageViewJob->onConnection('redis');
        
        dispatch($deletePageViewJob);
        
        $pageView->status = PageView::STATUS_DELETING;
        $pageView->save();
//        $pageView->delete();
        
        return response()->json([
            'success' => true
        ]);
    }

}