<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
// use Illuminate\Http\Request;
use App\Http\Requests\StoreProjectRequest;

class ApiProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->projects()->orderBy('created_at', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(StoreProjectRequest $request)
    {
        $project = new Project;
        $project->title = $request->input('title');
        $project->creator_id = Auth::user()->id;
        $project->status = Project::STATUS_OK;
        $project->save();
        
        $project->users()->attach(Auth::user());

        Log::info('User ' . Auth::user()->id . ' create project ' . $project->id);
        
        return response()->json([
            'success' => true,
            'data' => $project
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProjectRequest $request, Project $project)
    {
        $project->title = $request->input('title');
        $project->save();

        Log::info('User ' . Auth::user()->id . ' update project ' . $project->id);
        
        return response()->json([
            'success' => true,
            'data' => $project
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        if (!$project->users()->get()->contains(Auth::user())) {
            abort(403);
        }

        Log::info('User ' . Auth::user()->id . ' delete project ' . $project->id);
        
        $project->delete();
        
        return response()->json([
            'success' => true
        ]);
    }
}
