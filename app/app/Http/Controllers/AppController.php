<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Project;

class AppController extends Controller {


    public function index() {
        return view('project.index')->with('user', Auth::user());
    }


    // public function index(Project $project) {
        
    //     if (!$project->users()->exists( Auth::user() )) {
    //         abort(403, 'Forbidden');
    //     }
        
    //     return view('project.index')
    //             ->with('project', $project)
    //             ->with('user', Auth::user());
    // }
	

}