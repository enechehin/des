<?php

namespace App\Http\Controllers;

use Socialite;
use Log;
use App\Models\Auth\{User, Social, Role};
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{

    
    // private $auth;
    
    
    // function __construct(Guard $auth) {
    //     $this->auth = $auth;
    // }
    
    
    public function login() {
        return view('auth.login');
    }
    
    
    public function logout() {

        if (\Auth::check()) {
            \Auth::logout();
        }

        return redirect()->route('auth.login');
    }


    public function redirectToProvider() {
        
        $scopes = ['openid', 'email', 'profile', \Google_Service_Drive::DRIVE_FILE ];
        $params = ['access_type' => 'offline'];
        
        return Socialite::driver('google')
                ->scopes($scopes)
                ->with($params)
                ->redirect();
    }
    

    public function handleProviderCallback() {
        
        $provider = 'google';
        
        try {
            $user = Socialite::driver($provider)->user();
        }  catch (\Exception $e) {
            Log::error('Exception in auth@handleProviderCallback', [ 'message' => $e->getMessage() ]);
            return redirect()->route('auth.login');
        }
        
        $socialUser = null;
 
        //Check is this email present
        $userCheck = User::where('email', '=', $user->email)->first();
        
        if (!empty($userCheck)) {

            $socialUser = $userCheck;

        } else {
            
            $sameSocialId = Social::where('social_id', '=', $user->id)->where('provider', '=', $provider)->first();
            
            if (empty($sameSocialId)) {
                
                //There is no combination of this social id and provider, so create new one
                $newSocialUser = new User();
                $newSocialUser->email = $user->email;
                $newSocialUser->name = $user->name;

                $newSocialUser->save();
                
                $socialData = new Social();
                $socialData->social_id = $user->id;
                $socialData->provider = $provider;
                
                $newSocialUser->social()->save($socialData);
                
                // Add role
                $role = Role::whereName('user')->first();
                $newSocialUser->assignRole($role);
                
                $socialUser = $newSocialUser;
                
            } else {
                //Load this existing social user
                $socialUser = $sameSocialId->user;
            }
        }

        Auth::login($socialUser, true);
        
        if (Auth::user()->hasRole('user')) {
            return redirect()->route('app');
        }

        return \App::abort(500);
    }


}