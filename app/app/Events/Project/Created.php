<?php

namespace App\Events\Project;

use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Broadcasting\PrivateChannel;


class Created extends AbstractProjectEvent {



	/**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
    	return new PrivateChannel('user.' . $this->project->creator_id);
    }

}