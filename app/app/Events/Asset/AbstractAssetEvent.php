<?php

namespace App\Events\Asset;

use App\Models\Asset;
// use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
// use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

abstract class AbstractAssetEvent implements \Illuminate\Contracts\Broadcasting\ShouldBroadcast
{
    use Dispatchable, 
        InteractsWithSockets, 
        SerializesModels;

    public $asset;    


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->asset->project_id);
    }


    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return [
            'asset' => $this->asset
        ];
    }

}
