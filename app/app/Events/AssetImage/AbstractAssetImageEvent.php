<?php

namespace App\Events\AssetImage;

use App\Models\Assets\Image;
// use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
// use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

abstract class AbstractAssetImageEvent implements \Illuminate\Contracts\Broadcasting\ShouldBroadcast
{
    use Dispatchable, 
        InteractsWithSockets, 
        SerializesModels;

    public $image;    


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->image->project_id);
    }


    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return [
            'image' => $this->image
        ];
    }

}
