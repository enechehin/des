<?php

namespace App\Events\PageView;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Deleted implements \Illuminate\Contracts\Broadcasting\ShouldBroadcast
{
    use Dispatchable, 
        InteractsWithSockets, 
        SerializesModels;

    
    public $pageViewData = [];


    /**
     * Create a new event instance.
     *
     * @param $pageViewData array
     * @return void
     */
    public function __construct(array $pageViewData)
    {
        $this->pageViewData = $pageViewData;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->pageViewData['project_id']);
    }


    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return [
            'view' => $this->pageViewData
        ];
    }

}
