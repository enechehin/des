<?php

namespace App\Events\Page;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Deleted implements \Illuminate\Contracts\Broadcasting\ShouldBroadcast
{
    use Dispatchable, 
        InteractsWithSockets, 
        SerializesModels;

    
    public $pageData = [];


    /**
     * Create a new event instance.
     *
     * @param $pageData array
     * @return void
     */
    public function __construct(array $pageData)
    {
        $this->pageData = $pageData;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('project.' . $this->pageData['project_id']);
    }


    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return [
            'page' => $this->pageData
        ];
    }

}
