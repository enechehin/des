<?php

namespace App\Events\Source;

class Deleted extends AbstractSourceEvent {


}


//
//
//namespace App\Events\Source;
//
//use Illuminate\Queue\SerializesModels;
//use Illuminate\Broadcasting\PrivateChannel;
//use Illuminate\Foundation\Events\Dispatchable;
//use Illuminate\Broadcasting\InteractsWithSockets;
//use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
//
//class Deleted implements \Illuminate\Contracts\Broadcasting\ShouldBroadcast
//{
//    use Dispatchable, 
//        InteractsWithSockets, 
//        SerializesModels;
//
//    
//    public $sourceData = [];
//
//
//    /**
//     * Create a new event instance.
//     *
//     * @return void
//     */
//    public function __construct(array $sourceData)
//    {
//        $this->sourceData = $sourceData;
//    }
//
//
//    /**
//     * Get the channels the event should broadcast on.
//     *
//     * @return Channel|array
//     */
//    public function broadcastOn()
//    {
//        return new PrivateChannel('project.' . $this->sourceData['project_id']);
//    }
//
//
//    /**
//     * Get the data to broadcast.
//     *
//     * @return array
//     */
//    public function broadcastWith() {
//        return [
//            'source' => $this->sourceData
//        ];
//    }
//
//}
