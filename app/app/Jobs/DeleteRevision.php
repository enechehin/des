<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Revision;
use Illuminate\Support\Facades\Log;

class DeleteRevision implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $revision;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Revision $revision)
    {
        $this->revision = $revision;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Job DeleteRevision started', ['revision_id' => $this->revision->id]);
        
        $this->revision->delete();
        
        event(new \App\Events\PageView\RevisionsUpdated($this->revision->view));
        
        Log::info('Job DeleteRevision is done', ['revision_id' => $this->revision->id]);
    }
    
    
    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->revision->status = Revision::STATUS_OK;
        $this->revision->save();
    }
}
