<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Page;
use Illuminate\Support\Facades\Log;

class DeletePage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    protected $page;
    
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Page $page)
    {
        $this->page = $page;
    }
    

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Job DeletePage started', ['page_id' => $this->page->id]);
    
        $this->page->delete();
        
        Log::info('Job DeletePage is done', ['page_id' => $this->page->id]);
    }
    
    
    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->page->status = Page::STATUS_ERROR_DELETING;
        $this->page->save();
    }
    
}
