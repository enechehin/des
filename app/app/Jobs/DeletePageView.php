<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\PageView;
use Illuminate\Support\Facades\Log;

class DeletePageView implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    protected $pageView;
    
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PageView $pageView)
    {
        $this->pageView = $pageView;
    }
    

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Job DeletePageView started', ['pageview_id' => $this->pageView->id]);

        $this->pageView->delete();
        
        Log::info('Job DeletePageView is done', ['pageview_id' => $this->pageView->id]);
    }
    
    
    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $this->pageView->status = PageView::STATUS_OK;
        $this->pageView->save();
    }
    
}
