<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Revision;
use App\Models\Source;
use App\Models\Asset;
use App\Builders\ImageBuilder;
//use Illuminate\Support\Facades\Storage;

// use Illuminate\Support\Facades\DB;

class ExtractAssets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $revision;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Revision $revision)
    {
        $this->revision = $revision;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // DB::connection()->enableQueryLog();
        Log::info('Job ExtractAssets started', ['revision_id' => $this->revision->id]);

        $sources = $this->revision->sources;

        if (!$sources->count()) {
            Log::warn('Job ExtractAssets failed - no sources for extract', ['revision_id' => $this->revision->id]);
            return;
        }


        $this->setRevisionStatus(Revision::STATUS_EXTRACTING);

        $localSources = $sources->where('disk', Source::DISK_LOCAL);

        if (!$localSources->count()) {
            Log::info('Job ExtractAssets skiped - no local sources', ['revision_id' => $this->revision->id]);
            return;
        }
        
        $asset = Asset::create([
            'project_id' => $this->revision->project_id
        ]);

        $this->extractSourceToAsset($localSources->first(), $asset);

        // delete old and associate new asset
        $this->revision->asset()->delete();
        $this->revision->asset()->associate($asset);
       
        // sleep(5);
        $this->setRevisionStatus(Revision::STATUS_OK);

        Log::info('Job ExtractAssets is done', ['revision_id' => $this->revision->id]);
        // Log::debug('queries', DB::getQueryLog());
    }


    /**
     * @param $status int
     */
    private function setRevisionStatus(int $status) 
    {
        $this->revision->status = $status;
        $this->revision->save();
    }



    private function extractSourceToAsset(Source $source, Asset $asset) {

        Log::info('Job ExtractAssets proccess source', [
            'revision_id' => $source->revision_id,
            'source_id' => $source->id
        ]);

        $imageBuilder = new ImageBuilder($asset);

        if (in_array($source->mime_type, ['image/png', 'image/jpg', 'image/jpeg'])) {
            $image = $imageBuilder->createFromFile($source->fullyQualifiedPathname());
            $asset->images()->save($image);
        }
        
        if (in_array($source->mime_type, ['image/vnd.adobe.photoshop', 'image/psd', 'application/x-photoshop'])) {
          
        }

    }


    /**
     * Validate uploaded image document
     * return exif image type or false
     *
     * @return int|boolean
     */
    // private function validateUploadedFile($file) {

    //     $imageType = exif_imagetype($file);

    //     if ($imageType === false) {
    //         return false;
    //     }

    //     if (!in_array($imageType, [IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_PSD])) {
    //         return false;
    //     }

    //     return $imageType;
    // }


    /**
     * Process simple jpg and png images
     */
    // private function processSimpleImage() {

    // }


}
