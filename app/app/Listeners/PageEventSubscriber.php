<?php

namespace App\Listeners;

use App\Models\Page;
// use App\Models\Revision;
use App\Events\Page\Deleted;

class PageEventSubscriber
{

       
    /**
     * Handle page deleting event
     */
    public function onPageDeleting($event) 
    {
        if ($event->page->status !== Page::STATUS_DELETING) {
            $event->page->status = Page::STATUS_DELETING;
            $event->page->save();
        }
        
        $event->page->views->each(function($pageView){
            $pageView->delete();
        });

        event(new Deleted($event->page->makeVisible('project_id')->toArray()));
    }
    

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Page\Deleting',
            'App\Listeners\PageEventSubscriber@onPageDeleting'
        );
    }

}