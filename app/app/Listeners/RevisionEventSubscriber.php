<?php

namespace App\Listeners;

use App\Models\Revision;
use App\Events\Revision\Deleted;
use App\Events\PageView\RevisionsUpdated as PageViewRevisionsUpdated;

//use App\Jobs\ProcessUploadedRevision;

class RevisionEventSubscriber
{

    /**
     * Handle revision created event
     */
    public function onRevisionCreated($event) 
    {
        
    }


    /**
     * Handle revision updated event
     */
    public function onRevisionUpdated($event) 
    {
        event(new PageViewRevisionsUpdated($event->revision->view));
    }


    /**
     * Handle revision deleting event
     */
    public function onRevisionDeleting($event) 
    {
        if ($event->revision->status !== Revision::STATUS_DELETING) {
            $event->revision->status = Revision::STATUS_DELETING;
            $event->revision->save();
        }
        
        $event->revision->sources->each(function($source){
            $source->delete();
        });

        if ($event->revision->asset) {
            $event->revision->asset->delete();
        }
        
        event(new Deleted($event->revision->makeVisible('project_id')->toArray()));
    }



    /**
     * Handle revision uplaoded event
     */
    // public function onRevisionUploaded($event) 
    // {
    //     $event->revision->status = Revision::STATUS_UPLOADED;
    //     $event->revision->save();

    //     // Create async job for process uploaded revision file
    //     // and send it to redis queue
    //     $processUploadedRevisionJob = new ProcessUploadedRevision($event->revision, $event->file);
    //     $processUploadedRevisionJob->onConnection('redis');

    //     dispatch($processUploadedRevisionJob);
    // }


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Revision\Created',
            'App\Listeners\RevisionEventSubscriber@onRevisionCreated'
        );

        $events->listen(
            'App\Events\Revision\Updated',
            'App\Listeners\RevisionEventSubscriber@onRevisionUpdated'
        );

        $events->listen(
            'App\Events\Revision\Deleting',
            'App\Listeners\RevisionEventSubscriber@onRevisionDeleting'
        );

    }

}