<?php

namespace App\Listeners;

use App\Models\Assets\Image;
// use App\Models\Revision;
//use App\Events\Page\Deleted;
use Illuminate\Support\Facades\Storage;

class AssetEventSubscriber
{

       
    /**
     * Handle page deleting event
     */
    public function onDeleting($event) 
    {
        $event->asset->images->each(function(Image $image) {
            $image->delete();
        });
        
        Storage::deleteDirectory($event->asset->path());
    }
    

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Asset\Deleting',
            'App\Listeners\AssetEventSubscriber@onDeleting'
        );
    }

}