<?php

namespace App\Listeners;

//use App\Models\Assets\Image;
// use App\Models\Revision;
//use App\Events\Page\Deleted;
use Illuminate\Support\Facades\Storage;

class AssetImageEventSubscriber
{

       
    /**
     * Handle page deleting event
     */
    public function onImageDeleting($event) 
    {
        Storage::delete($event->image->pathname());
    }
    

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\AssetImage\Deleting',
            'App\Listeners\AssetImageEventSubscriber@onImageDeleting'
        );
    }

}