<?php

namespace App\Listeners;

use App\Models\PageView;
use App\Models\Revision;
use App\Events\PageView\Deleted;

class PageViewEventSubscriber
{

   
    // public function onPageViewCreated($event) 
    // {
        
    // }


    
    // public function onPageViewUpdated($event) 
    // {
        
    //     // Update status and revisions count
    //     $revisions = $event->pageView->revisions;

    //     $activeRevisionsCount = 
    //     $event->revision->view->updateCountRevisions();
    // }


    
    public function onPageViewRevisionsUpdated($event) 
    {
        
        /**
         * Update status and revisions count
         */
        $revisions = $event->pageView->revisions;

        $event->pageView->count_revisions = $revisions->count();
        $event->pageView->latest_revision = $revisions->count() ? $revisions->last()->id : 0;

        // updating revisions
        $updatingRevisionsCount = $event->pageView->revisions()->whereIn('status', [
            Revision::STATUS_UPLOADING
        ])->count();

        if ($updatingRevisionsCount) {
            $event->pageView->status = PageView::STATUS_OK_UPDATING;
        } else {
            $event->pageView->status = PageView::STATUS_OK;
        }

        $event->pageView->save();
    }

    
    /**
     * Handle pageview deleting event
     */
    public function onPageViewDeleting($event) 
    {
        if ($event->pageView->status !== PageView::STATUS_DELETING) {
            $event->pageView->status = PageView::STATUS_DELETING;
            $event->pageView->save();
        }
        
        $event->pageView->revisions->each(function($revisions){
            $revisions->delete();
        });

        event(new Deleted($event->pageView->makeVisible('project_id')->toArray()));
    }
    

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        // $events->listen(
        //     'App\Events\Revision\Created',
        //     'App\Listeners\RevisionEventSubscriber@onRevisionCreated'
        // );

        // $events->listen(
        //     'App\Events\PageView\Updated',
        //     'App\Listeners\PageViewEventSubscriber@onPageViewUpdated'
        // );

        $events->listen(
            'App\Events\PageView\RevisionsUpdated',
            'App\Listeners\PageViewEventSubscriber@onPageViewRevisionsUpdated'
        );
        
        $events->listen(
            'App\Events\PageView\Deleting',
            'App\Listeners\PageViewEventSubscriber@onPageViewDeleting'
        );
    }

}