<?php

namespace App\Listeners;

use App\Models\Source;
// use App\Events\PageView\RevisionsUpdated as PageViewRevisionsUpdated;
// use App\Jobs\ProcessUploadedRevision;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Events\Revision\Updated as RevisionUpdated;

class SourceEventSubscriber
{

    /**
     * Trigger parents updating
     * 
     * @param Source $source
     * @return array|null
     */
    private function triggerParentRevisionUpdated(Source $source) {
        return event(new RevisionUpdated($source->revision));
    }
    
    /**
     * Handle revision created event
     */
    public function onSourceCreated($event) 
    {
        $this->triggerParentRevisionUpdated($event->source);
    }


    /**
     * Handle revision updated event
     */
    public function onSourceUpdated($event) 
    {
//        $this->triggerParentRevisionUpdated($event->source);
        // event(new PageViewRevisionsUpdated($event->revision->view));
    }



    public function onSourceDeleting($event) 
    {
        $disk = $event->source->disk;

        $result = false;
        
        switch($disk) {

            case Source::DISK_LOCAL:
                $result = $this->deleteFromLocalDisk($event->source);
                break;

            default:
                throw new Exception('Can not delete from disk "' . $disk . '"');
                
        }

        return $result;
    }


    private function deleteFromLocalDisk(Source $source) {
        $result = Storage::delete($source->pathname());
        Log::info('Deleting source file from local disk', [
            'source_id' => $source->id,
            'result' => (int)$result
        ]);
        return $result;
    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Source\Created',
            'App\Listeners\SourceEventSubscriber@onSourceCreated'
        );

        $events->listen(
            'App\Events\Source\Updated',
            'App\Listeners\SourceEventSubscriber@onSourceUpdated'
        );

        $events->listen(
            'App\Events\Source\Deleting',
            'App\Listeners\SourceEventSubscriber@onSourceDeleting'
        );
        
    }

}