<?php


// Public pages
Route::get('/', function () {
    return view('welcome');
})->name('homepage');

Route::get('/home', 'HomeController@index');
Route::get('/login', ['uses' => 'AuthController@login'])->name('auth.login');
Route::post('/logout', ['uses' => 'AuthController@logout'])->name('auth.logout');

Route::group(['prefix' => 'auth'], function(){
    Route::get('redirect',   ['as' => 'auth.redirect',   'uses' => 'AuthController@redirectToProvider']);
    Route::get('handle',     ['as' => 'auth.handle',     'uses' => 'AuthController@handleProviderCallback']);
});


// Only for auth users
Route::group(['middleware' => ['auth', 'web']], function(){
    
    // App mainpage
    Route::get('/app/', 'AppController@index')->name('app');

    // API routes
    Route::group(['prefix' => 'api'], function(){

        // projects
        Route::get('projects', 'ApiProjectController@index');
        Route::post('projects', 'ApiProjectController@create');
        Route::put('projects/{project}', 'ApiProjectController@update');
        Route::delete('projects/{project}', 'ApiProjectController@destroy');

        // pages
        Route::get('projects/{project}/pages', 'ApiPageController@index');
        Route::post('projects/{project}/pages', 'ApiPageController@create');
        Route::put('pages/{page}', 'ApiPageController@update');
        Route::delete('pages/{page}', 'ApiPageController@destroy');

        // page views
        Route::get('pages/{page}/views', 'ApiPageViewController@index');
        Route::post('pages/{page}/views', 'ApiPageViewController@create');
        Route::put('views/{pageView}', 'ApiPageViewController@update');
        Route::delete('views/{pageView}', 'ApiPageViewController@destroy');

        // revisions
        Route::get('views/{pageView}/revisions', 'ApiRevisionController@index');
        Route::post('views/{pageView}/revisions', 'ApiRevisionController@create');
        Route::put('revisions/{revision}', 'ApiRevisionController@update');
        Route::delete('revisions/{revision}', 'ApiRevisionController@destroy');
        Route::get('revisions/{revision}/asset', 'ApiRevisionController@asset');

    });

    // Handle uploads
    Route::post('/upload/{revisionId}', 'SourceController@upload')->where('revisionId', '[0-9]+');

    // Handle source download
    Route::get('/download/{revision}', 'SourceController@download');
    
});
