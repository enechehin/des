<?php

use App\Models\Project;



Broadcast::channel('user.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('project.{project}', function ($user, Project $project) {
	return $project->users()->exists( $user );
});