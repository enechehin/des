import * as types from '../mutation-types'

// initial state
const state = {
  user: window.data.user
}

// getters
const getters = {

}

// actions
const actions = {

}

// mutations
const mutations = {

}

export default {
  state,
  getters,
  actions,
  mutations
}
