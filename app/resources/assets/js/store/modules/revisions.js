import { set, delete as vueDelete } from 'vue'
import revisionsApi from '../../api/revision'
import * as types from '../mutation-types'
import { events, 
  REMOTE_REVISION_CREATED, REMOTE_REVISION_UPDATED, REMOTE_REVISION_DELETED, 
  REMOTE_SOURCE_CREATED, REMOTE_SOURCE_UPDATED, REMOTE_SOURCE_DELETED } from '../../utils/events';

// initial state
const state = {
  all: {},
  loading: false,
  currentRevisionId: ''
}

// getters
const getters = {
  currentRevision: state => state.currentRevisionId ? state.all[state.currentRevisionId] : {},
  countRevisions: state => _.size(state.all),
}

// actions
const actions = {

  loadRevisions ({ commit, rootState }) {
    commit(types.REVISIONS_LOADING_ON)
    revisionsApi.getAll(rootState.pages.currentViewId, revisions => {
      commit(types.SET_REVISIONS, revisions)
      commit(types.REVISIONS_LOADING_OFF)
    })
  },

  initRevisionsEvents({ commit, rootState }) {

    events.on(REMOTE_REVISION_CREATED, function(e){
      if (rootState.pages.currentViewId == e.revision.pageview_id) {
        commit(types.PUSH_REVISION, e.revision)
      }
    });

    events.on(REMOTE_REVISION_UPDATED, function(e){
      if (rootState.pages.currentViewId == e.revision.pageview_id) {
        commit(types.UPDATE_REVISION, e.revision)
      }
    });

    events.on(REMOTE_REVISION_DELETED, function(e){
      if (rootState.pages.currentViewId == e.revision.pageview_id) {
        commit(types.DELETE_REVISION, e.revision)
      }
    });

    // events.on(REMOTE_SOURCE_CREATED, function(e){
    //   console.log(REMOTE_SOURCE_CREATED, typeof state.all[e.source.revision_id],state.all[e.source.revision_id]);
    //   if (typeof state.all[e.source.revision_id] !== 'undefined') {
    //   // if (rootState.pages.currentViewId == e.source.pageview_id) {
    //     commit(types.PUSH_SOURCE, e.source)
    //   }
    // });

    // events.on(REMOTE_SOURCE_UPDATED, function(e){
    //   if (rootState.pages.currentViewId == e.source.pageview_id) {
    //     commit(types.UPDATE_SOURCE, e.revision)
    //   }
    // });

    // events.on(REMOTE_SOURCE_DELETED, function(e){
    //   if (rootState.pages.currentViewId == e.source.pageview_id) {
    //     commit(types.DELETE_SOURCE, e.revision)
    //   }
    // });

  },

  switchRevision({ commit }, revisionId) {
    commit(types.CHANGE_CURRENT_REVISION, revisionId);
  }

}

// mutations
const mutations = {

  [types.SET_REVISIONS] (state, revisions) {
    state.all = {};
    revisions.forEach(revision => {
      set(state.all, revision.id, revision);
    })
  },

  [types.PUSH_REVISION] (state, revision) {
    set(state.all, revision.id, revision);
  },

  [types.UPDATE_REVISION] (state, revision) {
    set(state.all, revision.id, revision);
  },

  [types.DELETE_REVISION] (state, revision) {
    vueDelete(state.all, revision.id);
  },

  [types.REVISIONS_LOADING_ON] (state) {
    state.loading = true
  },

  [types.REVISIONS_LOADING_OFF] (state) {
    state.loading = false
  },

  [types.CHANGE_CURRENT_REVISION] (state, revisionId) {
    state.currentRevisionId = revisionId;
  },

  // [types.PUSH_SOURCE] (state, source) {
  //   state.all[source.revision_id].sources.push(source);
  //   // set(state.all[source.revision_id].sources, source.id, source);
  // },

}

export default {
  state,
  getters,
  actions,
  mutations
}
