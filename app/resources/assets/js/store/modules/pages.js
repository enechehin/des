import { set, delete as vueDelete } from 'vue'
import pageApi from '../../api/page'
import * as types from '../mutation-types'
import { events, 
  REMOTE_PAGE_CREATED, 
  REMOTE_PAGE_UPDATED, 
  REMOTE_PAGE_DELETING,
  REMOTE_PAGE_DELETED,
  REMOTE_PAGEVIEW_CREATED, 
  REMOTE_PAGEVIEW_UPDATED, 
  REMOTE_PAGEVIEW_DELETED
} from '../../utils/events';

// initial state
const state = {
  all: {},
  loading: false,
  currentPageId: '',
  currentViewId: null,
}

// getters
const getters = {
  currentPage: state => state.currentPageId ? state.all[state.currentPageId] : {},
  countPages: state => _.size(state.all),
  currentView(state, getters){

    if (!state.currentPageId || !state.currentViewId) {
      return null;
    }

    const page = getters.currentPage;

    if (!page) {
      return null;
    }

    return page.views.find((item) => item.id == state.currentViewId);
  } 
}

// actions
const actions = {

  loadPages ({ commit, rootState }, projectId) {

    if (!rootState.projects.currentProjectId) {
      commit(types.SET_PAGES, {});
      return false;
    }

    commit(types.PAGES_LOADING_ON)

    pageApi.getAll(rootState.projects.currentProjectId, pages => {
      commit(types.SET_PAGES, pages)
      commit(types.PAGES_LOADING_OFF)
    })
  },

  initPagesEvents({ commit }) {

    events.on(REMOTE_PAGE_CREATED, function(e){
      commit(types.PUSH_PAGE, e.page)
    });

    events.on(REMOTE_PAGE_UPDATED, function(e){
      commit(types.UPDATE_PAGE, e.page)
    });

    events.on(REMOTE_PAGE_DELETING, function(e){
      commit(types.UPDATE_PAGE, e.page)
    });

    events.on(REMOTE_PAGE_DELETED, function(e){
      commit(types.DELETE_PAGE, e.page)
    });

    events.on(REMOTE_PAGEVIEW_CREATED, function(e){
      commit(types.PUSH_PAGEVIEW, e.view)
    });

    events.on(REMOTE_PAGEVIEW_UPDATED, function(e){
      commit(types.UPDATE_PAGEVIEW, e.view)
    });

    events.on(REMOTE_PAGEVIEW_DELETED, function(e){
      commit(types.DELETE_PAGEVIEW, e.view)
    });

  },

  switchView({ commit }, { pageId, viewId }) {
    return new Promise((resolve, reject) => {
      commit(types.CHANGE_CURRENT_PAGE, pageId);
      commit(types.CHANGE_CURRENT_PAGEVIEW, viewId);
      resolve();
    });
  }

}

// mutations
const mutations = {

  [types.SET_PAGES] (state, pages) {
    pages.forEach(page => {
      set(state.all, page.id, page);
    })
  },

  [types.PUSH_PAGE] (state, page) {
    set(state.all, page.id, page);
  },

  [types.UPDATE_PAGE] (state, page) {
    set(state.all, page.id, page);
  },

  [types.DELETE_PAGE] (state, page) {
    vueDelete(state.all, page.id);
  },

  [types.PAGES_LOADING_ON] (state) {
    state.loading = true
  },

  [types.PAGES_LOADING_OFF] (state) {
    state.loading = false
  },

  [types.CHANGE_CURRENT_PAGE] (state, pageId) {
    state.currentPageId = pageId;
  },

  [types.PUSH_PAGEVIEW] (state, view) {
    state.all[view.page_id].views.push(view);
  },

  [types.UPDATE_PAGEVIEW] (state, view) {
    const viewIndex = state.all[view.page_id].views.findIndex((item) => item.id == view.id);
    Vue.set(state.all[view.page_id].views, viewIndex, view);
  },

  [types.DELETE_PAGEVIEW] (state, view) {
    const viewIndex = state.all[view.page_id].views.findIndex((item) => item.id == view.id);
    state.all[view.page_id].views.splice(viewIndex, 1);
  },

  [types.CHANGE_CURRENT_PAGEVIEW] (state, viewId) {
    // console.log('CHANGE_CURRENT_PAGEVIEW', viewId);
    state.currentViewId = viewId;
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}
