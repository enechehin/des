import { set, delete as vueDelete } from 'vue'
import projectsApi from '../../api/project'
import * as types from '../mutation-types'
import { events, REMOTE_PROJECT_CREATED, REMOTE_PROJECT_UPDATED, REMOTE_PROJECT_DELETED, PROJECT_SWITHED } from '../../utils/events';

// initial state
const state = {
  all: {},
  loading: false,
  currentProjectId: ''
}

// getters
const getters = {
  currentProject: state => state.currentProjectId ? state.all[state.currentProjectId] : {}
}

// actions
const actions = {

  loadProjects ({ commit }) {
    commit(types.PROJECTS_LOADING_ON)
    projectsApi.getAll(projects => {
      commit(types.SET_PROJECTS, projects)
      commit(types.PROJECTS_LOADING_OFF)
    })
  },

  initProjectsEvents({ commit }) {

    events.on(REMOTE_PROJECT_CREATED, function(e){
      commit(types.PUSH_PROJECT, e.project)
    });

    events.on(REMOTE_PROJECT_UPDATED, function(e){
      commit(types.UPDATE_PROJECT, e.project)
    });

    events.on(REMOTE_PROJECT_DELETED, function(e){
      commit(types.DELETE_PROJECT, e.project)
    });

  },

}

// mutations
const mutations = {

  [types.SET_PROJECTS] (state, projects) {
    projects.forEach(project => {
      set(state.all, project.id, project);
    })
  },

  [types.PUSH_PROJECT] (state, project) {
    set(state.all, project.id, project);
  },

  [types.UPDATE_PROJECT] (state, project) {
    set(state.all, project.id, project);
  },

  [types.DELETE_PROJECT] (state, project) {
    vueDelete(state.all, project.id);
  },

  [types.PROJECTS_LOADING_ON] (state) {
    state.loading = true
  },

  [types.PROJECTS_LOADING_OFF] (state) {
    state.loading = false
  },

  [types.CHANGE_CURRENT_PROJECT] (state, projectId) {
    state.currentProjectId = projectId;
  },

}

export default {
  state,
  getters,
  actions,
  mutations
}
