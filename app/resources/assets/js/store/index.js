import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import auth from './modules/auth'
import projects from './modules/projects'
import pages from './modules/pages'
import revisions from './modules/revisions'
// import createLogger from '../../../src/plugins/logger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  actions,
  getters,
  modules: {
  	auth,
    projects,
    pages,
    revisions
  },
  strict: debug,
  // plugins: debug ? [createLogger()] : []
})
