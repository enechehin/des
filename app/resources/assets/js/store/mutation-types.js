// export const ADD_TO_CART = 'ADD_TO_CART'
// export const CHECKOUT_REQUEST = 'CHECKOUT_REQUEST'
// export const CHECKOUT_SUCCESS = 'CHECKOUT_SUCCESS'
// export const CHECKOUT_FAILURE = 'CHECKOUT_FAILURE'
// export const RECEIVE_PRODUCTS = 'RECEIVE_PRODUCTS'

export const SET_PROJECTS = 'SET_PROJECTS'
export const PUSH_PROJECT = 'PUSH_PROJECT'
export const UPDATE_PROJECT = 'UPDATE_PROJECT'
export const DELETE_PROJECT = 'DELETE_PROJECT'
export const PROJECTS_LOADING_ON = 'PROJECTS_LOADING_ON'
export const PROJECTS_LOADING_OFF = 'PROJECTS_LOADING_OFF'
export const CHANGE_CURRENT_PROJECT = 'CHANGE_CURRENT_PROJECT'

export const SET_PAGES = 'SET_PAGES'
export const PUSH_PAGE = 'PUSH_PAGE'
export const UPDATE_PAGE = 'UPDATE_PAGE'
export const DELETE_PAGE = 'DELETE_PAGE'
export const PAGES_LOADING_ON = 'PAGES_LOADING_ON'
export const PAGES_LOADING_OFF = 'PAGES_LOADING_OFF'
export const CHANGE_CURRENT_PAGE = 'CHANGE_CURRENT_PAGE'

export const PUSH_PAGEVIEW = 'PUSH_PAGEVIEW'
export const UPDATE_PAGEVIEW = 'UPDATE_PAGEVIEW'
export const DELETE_PAGEVIEW = 'DELETE_PAGEVIEW'

export const CHANGE_CURRENT_PAGEVIEW = 'CHANGE_CURRENT_PAGEVIEW'

export const SET_REVISIONS = 'SET_REVISIONS'
export const PUSH_REVISION = 'PUSH_REVISION'
export const UPDATE_REVISION = 'UPDATE_REVISION'
export const DELETE_REVISION = 'DELETE_REVISION'
export const REVISIONS_LOADING_ON = 'REVISIONS_LOADING_ON'
export const REVISIONS_LOADING_OFF = 'REVISIONS_LOADING_OFF'
export const CHANGE_CURRENT_REVISION = 'CHANGE_CURRENT_REVISION'

export const PUSH_SOURCE = 'PUSH_SOURCE'
export const UPDATE_SOURCE = 'UPDATE_SOURCE'
export const DELETE_SOURCE = 'DELETE_SOURCE'