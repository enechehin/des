import Vue from 'vue'
import store from '../store'
import { each } from 'lodash'


export const REMOTE_PROJECT_CREATED = 'remote.project.created'
export const REMOTE_PROJECT_UPDATED = 'remote.project.updated'
export const REMOTE_PROJECT_DELETED = 'remote.project.deleted'
export const PROJECT_SWITHED = 'project.switched'

export const REMOTE_PAGE_CREATED = 'remote.page.created'
export const REMOTE_PAGE_UPDATED = 'remote.page.updated'
export const REMOTE_PAGE_DELETING = 'remote.page.deleting'
export const REMOTE_PAGE_DELETED = 'remote.page.deleted'

export const REMOTE_PAGEVIEW_CREATED = 'remote.pageview.created'
export const REMOTE_PAGEVIEW_UPDATED = 'remote.pageview.updated'
// export const REMOTE_PAGEVIEW_DELETING = 'remote.pageview.deleting'
export const REMOTE_PAGEVIEW_DELETED = 'remote.pageview.deleted'

export const REMOTE_REVISION_CREATED = 'remote.revision.created'
export const REMOTE_REVISION_UPDATED = 'remote.revision.updated'
export const REMOTE_REVISION_DELETING = 'remote.revision.deleting'
export const REMOTE_REVISION_DELETED = 'remote.revision.deleted'

export const REMOTE_SOURCE_CREATED = 'remote.source.created'
export const REMOTE_SOURCE_UPDATED = 'remote.source.updated'
export const REMOTE_SOURCE_DELETED = 'remote.source.deleted'


/**
 * A simple event bus.
 *
 * @type {Object}
 */
const events = {
  bus: null,
  projectChannel: false,

  init () {
    if (!this.bus) {
      this.bus = new Vue()
    }

    return this
  },

  emit (name, ...args) {
    this.bus.$emit(name, ...args)
    return this
  },

  on () {
    if (arguments.length === 2) {
      this.bus.$on(arguments[0], arguments[1])
    } else {
      each(Object.keys(arguments[0]), key => this.bus.$on(key, arguments[0][key]))
    }

    return this
  },

  off (name, ...args) {
    this.bus.$off(name, ...args)
    return this
  },

  listenUserBroadcastEvents() {

    Echo.private('user.' + store.state.auth.user.id)
      .listen('Project.Created', (payload) => {
          events.emit(REMOTE_PROJECT_CREATED, payload);
      })
      .listen('Project.Updated', (payload) => {
          events.emit(REMOTE_PROJECT_UPDATED, payload);
      })
      .listen('Project.Deleted', (payload) => {
          events.emit(REMOTE_PROJECT_DELETED, payload);
      }); 

  },

  listenProjectBroadcastEvents() {

    this.destroyProjectBroadcastEvents();

    this.projectChannel = 'project.' + store.state.projects.currentProjectId;

    Echo.private(this.projectChannel)
      .listen('Page.Created', (payload) => {
          events.emit(REMOTE_PAGE_CREATED, payload);
      })
      .listen('Page.Updated', (payload) => {
          events.emit(REMOTE_PAGE_UPDATED, payload);
      })
      .listen('Page.Deleting', (payload) => {
          events.emit(REMOTE_PAGE_DELETING, payload);
      })  
      .listen('Page.Deleted', (payload) => {
          events.emit(REMOTE_PAGE_DELETED, payload);
      })
      .listen('PageView.Created', (payload) => {
          events.emit(REMOTE_PAGEVIEW_CREATED, payload);
      })
      .listen('PageView.Updated', (payload) => {
          events.emit(REMOTE_PAGEVIEW_UPDATED, payload);
      })
      .listen('PageView.RevisionsUpdated', (payload) => {
          events.emit(REMOTE_PAGEVIEW_UPDATED, payload);
      })
      .listen('PageView.Deleting', (payload) => {
          events.emit(REMOTE_PAGEVIEW_UPDATED, payload);
          // events.emit(REMOTE_PAGEVIEW_DELETED, payload);
      })
      .listen('PageView.Deleted', (payload) => {
          events.emit(REMOTE_PAGEVIEW_DELETED, payload);
      })
      .listen('Revision.Created', (payload) => {
          events.emit(REMOTE_REVISION_CREATED, payload);
      })
      .listen('Revision.Updated', (payload) => {
          events.emit(REMOTE_REVISION_UPDATED, payload);
      })
      .listen('Revision.Deleting', (payload) => {
          events.emit(REMOTE_REVISION_DELETING, payload);
      })
      .listen('Revision.Deleted', (payload) => {
          events.emit(REMOTE_REVISION_DELETED, payload);
      })
      .listen('Source.Created', (payload) => {
          events.emit(REMOTE_SOURCE_CREATED, payload);
      })
      .listen('Source.Updated', (payload) => {
          events.emit(REMOTE_SOURCE_UPDATED, payload);
      })
      .listen('Source.Deleting', (payload) => {
          events.emit(REMOTE_SOURCE_DELETED, payload);
      })
      .listen('Source.Deleted', (payload) => {
          events.emit(REMOTE_SOURCE_DELETED, payload);
      });  

  },

  destroyProjectBroadcastEvents() {
    if (this.projectChannel) {
      Echo.leave(this.projectChannel);
      this.projectChannel = false;
    }
  }

}

export { events }


