import store from '../store'

export default  {

	init() {
		
		Echo.private('user.' + store.state.auth.user.id)
	    	.notification((notification) => {
	        	console.log(notification.type, notification);
	    	});
	}

};