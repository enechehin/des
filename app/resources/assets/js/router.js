import VueRouter from 'vue-router';
import store from './store'

import { events, PROJECT_SWITHED } from './utils/events';

import projectsCrud from './components/projects/index.vue';

import ProjectLayout from './components/ProjectLayout/index.vue';
import ProjectIndex from './components/ProjectIndex/index.vue';
import PageView from './components/PageView/index.vue';
import ViewUploader from './components/ViewUploader/index.vue';
import RevisionViewer from './components/RevisionViewer/index.vue';



import * as types from './store/mutation-types'


const projectRoutes = [
    {
        path: '', 
        name: 'project',
        component: ProjectIndex 
    },
    {
        path: 'v/:pageId(\\d+)/:viewId(\\d+)/',
        name: 'project.view',
        component: PageView, 
        beforeEnter: function(to, from, next){ 
            store.dispatch('switchView', { pageId: to.params.pageId, viewId: to.params.viewId });
            return next();
        },
        children: [

            {
                path: 'upload',
                name: 'project.view.upload',
                component: ViewUploader,
                beforeEnter: function(to, from, next){ 
                    console.log('project.view.upload');
                    store.dispatch('switchRevision', null);
                    return next();
                },
            },

            {
                path: ':revisionId(\\d+)',
                name: 'project.view.revision',
                component: RevisionViewer,
                beforeEnter: function(to, from, next){ 
                    store.dispatch('switchRevision', to.params.revisionId);
                    return next();
                },
            }

        ]
    }
];

  
export default new VueRouter({
    mode: 'hash',
    routes: [
        { 
            path: '/', 
            name: 'projects', 
            component: projectsCrud, 
            beforeEnter: function(to, from, next){ 
            	store.commit(types.CHANGE_CURRENT_PROJECT, '');
                events.emit(PROJECT_SWITHED);
            	return next();
            } 
        },
        { 
            path: '/:projectId(\\d+)/', 
            // name: 'project',
            component: ProjectLayout,
            beforeEnter: function(to, from, next){ 
            	store.commit(types.CHANGE_CURRENT_PROJECT, to.params.projectId);
                store.dispatch('switchView', { pageId: null, viewId: null });
                events.emit(PROJECT_SWITHED);
            	return next();
            }, 
            // redirect: {
            // 	name: 'project.pages', 
            // 	props: (route) => ({ projectId: route.props.projectId })
            // },
            children: projectRoutes
        },
    ] 
});
