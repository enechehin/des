import { Message } from 'element-ui';

export default {

  getAll (pageId, cb) {
    axios.get('/api/pages/' + pageId + '/views').then(function(response){
      cb(response.data);
    }).catch(function (error) {
      console.log(error);
      Message.error({
        message: 'Error while fetching pageviews'
      });
    });
  },

  createView(view, cb) {

  	if (!view.page_id) {
  		console.error('page_id is not setted !');
  		return false;
  	}

  	axios.post('/api/pages/' + view.page_id + '/views', view).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while pageview creating'
        });
    });
  },

  updateView(view, cb) {
  	axios.put('/api/views/' + view.id, view).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while pageview updating'
        });
    });
  },

  deleteView(view, cb) {
  	axios.delete('/api/views/' + view.id).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while pageview deleting'
        });
    });
  },


  STATUS: {
    OK: 1,
    OK_UPDATING: 2,
    DELETING: 3
  }

}