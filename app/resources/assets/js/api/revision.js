import { Message } from 'element-ui';

export default {

  getAll (viewId, cb) {
    axios.get('/api/views/' + viewId + '/revisions').then(function(response){
      cb(response.data);
    }).catch(function (error) {
      console.log(error);
      Message.error({
        message: 'Error while fetching revisions'
      });
    });
  },

  createRevision(revision, cb) {

  	if (!revision.pageview_id) {
  		console.error('createRevision: pageview_id is not setted !');
  		return false;
  	}

  	axios.post('/api/views/' + revision.pageview_id + '/revisions', revision).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while revision creating'
        });
    });
  },

  updateRevision(revision, cb) {

    if (!revision.id) {
      console.error('updateRevision: revision.id is not setted !');
      return false;
    }

  	axios.put('/api/revisions/' + revision.id, revision).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while revision updating'
        });
    });
  },

  deleteRevision(revision, cb) {
  	axios.delete('/api/revisions/' + revision.id).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while revision deleting'
        });
    });
  },

  getAsset(revision, cb) {
    axios.get('/api/revisions/' + revision.id + '/asset').then(function(response){
        cb(response.data);
    }).catch(function (error) {
        console.log(error);
        Message.error({
          message: 'Error while revision fetching asset'
        });
    });
  },

  STATUS: {
    OK: 1,
    UPLOADING: 2,
    UPLOADED: 3,
    EXTRACTING: 10,
    DELETING: 20,
    ERROR_UPLOAD: 101,
    ERROR_VALIDATE: 102
  }

}