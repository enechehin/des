import { Message } from 'element-ui';

export default {

  getAll (projectId, cb) {
    axios.get('/api/projects/' + projectId + '/pages').then(function(response){
      cb(response.data);
    }).catch(function (error) {
      console.log(error);
      Message.error({
        message: 'Error while fetching pages'
      });
    });
  },

  createPage(projectId, page, cb) {
  	axios.post('/api/projects/' + projectId + '/pages', page).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while page creating'
        });
    });
  },

  updatePage(page, cb) {
  	axios.put('/api/pages/' + page.id, page).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while page updating'
        });
    });
  },

  deletePage(page, cb) {
  	axios.delete('/api/pages/' + page.id).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while page deleting'
        });
    });
  },


  STATUS: {
    OK: 1,
    DELETING: 2
  }

}