import { Message } from 'element-ui';

export default {

  getAll (cb) {
    axios.get('/api/projects').then(function(response){
      cb(response.data);
    }).catch(function (error) {
      console.log(error);
      Message.error({
        message: 'Error while fetching projects'
      });
    });
  },

  saveProject(project, cb) {

  	axios.post('/api/projects', project).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while project creating'
        });
    });
  },

  updateProject(project, cb) {

  	if (!project.id) {
  		console.error('Project id is empty');
  	}

  	axios.put('/api/projects/' + project.id, project).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while project updating'
        });
    });
  },

  deleteProject(project, cb) {
  	if (!project.id) {
  		console.error('Project id is empty');
  	}

  	axios.delete('/api/projects/' + project.id).then(function(response){
      	cb(response.data);
    }).catch(function (error) {
      	console.log(error);
        Message.error({
          message: 'Error while project deleting'
        });
    });
  }

}