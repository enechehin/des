
require('./bootstrap');

import AppLayout from './components/AppLayout' 

// Vuex state store
import store from './store'

// Require router
import router from './router';

// Require events dispatcher
import { events } from './utils/events';

events.init();

// Require router
import notifications from './utils/notifications';


// Global components
// Vue.component('project-selector', require('./components/toolbar/project-selector'));


//Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(AppLayout),
    methods: {
      
    },
    created() {
        
        events.listenUserBroadcastEvents();

        notifications.init();

        this.$store.dispatch('initProjectsEvents');
        this.$store.dispatch('initPagesEvents');
        this.$store.dispatch('initRevisionsEvents');
        this.$store.dispatch('loadProjects');
    }
    
});
