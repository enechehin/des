<nav class="navbar navbar-default">
    
    <div class="container-fluid">
        
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            
            <!-- Left Side Of Navbar -->
            <!-- <form class="navbar-form navbar-left">
                <project-selector></project-selector>
            </form> -->

            <!-- Right Side Of Navbar -->

            <el-menu class="nav navbar-nav navbar-right" theme="dark" mode="horizontal" router>
                @if (Auth::guest())
                    <el-menu-item index="1"><a href="{{ route('auth.login') }}">Login</a></el-menu-item>
                @else
                    <el-submenu index="1">
                        <template slot="title">{{ Auth::user()->name }}</template>
                        <el-menu-item index="/">Projects</el-menu-item>
                        <el-menu-item index="1-2">
                            <a href="{{ route('auth.logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </el-menu-item>
                    </el-submenu>
 
                @endif
            </el-menu>

    </div>
    
</nav>    
