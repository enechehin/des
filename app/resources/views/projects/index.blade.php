@extends('layouts.app')

@section('content')

<projects-crud></projects-crud>

<!-- <div class="container">
    <div class="panel panel-default">

        <div class="panel-body">   
            
            <h1>My projects</h1>

            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif


            <table class="table">

                @foreach($projects as $project)

                    <tr>

                        <td><a href="{{ $project->getLink() }}">{{ $project->title }}</a></td>

                        <td>
                            
                            {{ Form::open(array('url' => route('projects.destroy', ['id' => $project->id]), 'class' => 'pull-right')) }}

                                {{ Form::hidden('_method', 'DELETE') }}
                                
                                <div class="btn-group">
                                    
                                    <a class="btn btn-sm btn-info" 
                                       accesskey=""href="{{ route('projects.edit', ['id' => $project->id]) }}">Edit</a>

                                    {{ Form::submit('Delete', array('class' => 'btn btn-sm btn-warning')) }}
                                    
                                </div>
                            {{ Form::close() }}
                            
                        </td>

                    </tr>

                @endforeach

            </table>
            
        </div>
        
        <div class="panel-footer">
            <a class="btn btn-primary" href="{{ route('projects.create') }}">Add</a>
        </div>
        
    </div>
</div> -->

@endsection