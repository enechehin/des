@extends('layouts.app')

@section('content')

<main>
    
    <router-view class="view"></router-view>
      
</main>

@endsection