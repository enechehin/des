<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.data = {!! json_encode([
            'appName' => config('app.name', 'Laravel'),
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
</head>
<body>
    
    <div id="app"></div>
        
    <!-- Scripts -->
    <script src="/js/app.js"></script>
    
</body>
</html>
